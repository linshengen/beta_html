//登录界面
$(".regin").click(function() {
	var name = $("#username").val();
	var psd = $("#password").val();
	var action = $("#action").val();

	var xmlhttp;
	if (window.XMLHttpRequest) {
		// IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
		xmlhttp = new XMLHttpRequest();
	} else {
		// IE6, IE5 浏览器执行代码
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var str = "";
			str = xmlhttp.responseText;
			alert(str);
			if (str == "登陆成功！") {
				window.location = "../index.html";
			}
		}
	}
	xmlhttp.open("POST", "http://ezcteam.xyz/ezcphp/test/public/index.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("name=" + name + "&pwd=" + psd + "&action=" + action+"&phone="+1);

})
//切换到注册界面
$(".signup").click(function() {
	$(".sign").show().siblings().hide();
})

$(".forgetup").click(function() {
	$(".forget").show().siblings().hide();
})
//验证用户名是否符合要求
$("#name").blur(function() {
		var reg = /^[a-zA-Z0-9\_]{9}$/
		var value = $(this).val();
		if (reg.test(value)) {
			$(".signName span").text("符合要求")
		} else {
			$(".signName span").text("9位学号");
		}
	})
//验证密码是否符合要求
	$("#pass").blur(function() {
		var reg = /^[a-zA-Z0-9\_]{8,16}$/
		var value = $(this).val();
		if (reg.test(value)) {
			$(".signPassword span").text("符合要求")
		} else {
			$(".signPassword span").text("密码少于8位");
		}
	})
//再次确认密码
	$("#pass1").blur(function() {
	
		var pass = $("#pass").val();
		var pass1 = $(this).val();
		if (pass1 == pass&&pass!="") {
			$(".signConfirm span").text("密码一致");
		} else {
			$(".signConfirm span").text("密码不相同");
		}
	})
//验证手机号是否正确
	$("#phone").blur(function() {
		
		var reg = /^1[3|4|5|7|8][0-9]{9}$/
		var value = $(this).val();
		if (reg.test(value)) {
			$(".signPhone span").text("符合要求")
		} else {
			$(".signPhone span").text("输入有误");
		}
	})
	
//验证验证码是否正确

$("#code1").blur(function() {
		
		var value = $(this).val();
		if (reg.test(value)) {
			$(".signCode span").text("符合要求")
		} else {
			$(".signCode span").text("输入有误");
		}
	})

//在注册界面返回到登录界面
$(".back").click(function() {
	$(".login").show().siblings().hide();
})
//忘记密码界面	
	//验证用户名是否符合要求
	$("#name1").blur(function() {
			var reg = /^[a-zA-Z0-9\_]{9}$/;
			// var reg = /^[0-9\_]{9}$/;
			var value = $(this).val();
			if (reg.test(value)) {
				$(".signName span").text("符合要求")
			} else {
				$(".signName span").text("请按要求填写");
			}
		})
		$("#name2").blur(function() {
				var reg = /^[a-zA-Z0-9\_]{10}$/
				var value = $(this).val();
				if (reg.test(value)) {
					$(".signName span").text("符合要求")
				} else {
					$(".signName span").text("请按要求填写");
				}
			})
	//验证密码是否符合要求
		$("#pass2").blur(function() {
			var reg = /^[a-zA-Z0-9\_]{3,12}$/
			var value = $(this).val();
			if (reg.test(value)) {
				$(".signPassword span").text("符合要求")
			} else {
				$(".signPassword span").text("请按要求填写");
			}
		})
	//再次确认密码
		$("#pass3").blur(function() {
		
			var pass = $("#pass").val();
			var pass1 = $(this).val();
			if (pass1 == pass) {
				$(".signConfirm span").text("密码一致");
			} else {
				$(".signConfirm span").text("密码不相同");
			}
		})

    window.onload=function(){
     createCode(5);    
    }

    //生成验证码的方法
    function createCode(length) {
        var code = "";
        var codeLength = parseInt(length); //验证码的长度
        var checkCode = document.getElementById("checkCode");
        ////所有候选组成验证码的字符，当然也可以用中文的
        var codeChars = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'); 
        //循环组成验证码的字符串
        for (var i = 0; i < codeLength; i++)
        {
            //获取随机验证码下标
            var charNum = Math.floor(Math.random() * 62);
            //组合成指定字符验证码
            code += codeChars[charNum];
        }
        if (checkCode)
        {
            //为验证码区域添加样式名
            checkCode.className = "code";
            //将生成验证码赋值到显示区
            checkCode.innerHTML = code;
        }
    }
  //   function sendCode(){
  //   	var keyPhone= $("#phone").val();
    	
  //   	var xmlhttp;
  //   	if (window.XMLHttpRequest) {
  //   		// IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
  //   		xmlhttp = new XMLHttpRequest();
  //   	} else {
  //   		// IE6, IE5 浏览器执行代码
  //   		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  //   	}
  //   	xmlhttp.onreadystatechange = function() {
  //   		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  //   			var str = "";
  //   			str = xmlhttp.responseText;
  //   			window.checkIdd=str;
  //   			console.log(window.checkIdd);
		// 		alert("验证码已发送至手机！");
  //   		}
    		
  //   	}
  //   	// xmlhttp.open("POST", "http://localhost/MVC/public/index.php", true);
		// xmlhttp.open("POST", "http://ezcteam.xyz/ezcphp/test/public/index.php", true);
  //   	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  //   	xmlhttp.send("keyPhone=" + keyPhone + "&action=sendms");		
  //   }
	
	function sendCode() {
		var keyPhone = $("#keyPhone").val();
		var val=document.getElementById("sendButton");
		var xmlhttp;
		if (window.XMLHttpRequest) {
			// IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
			xmlhttp = new XMLHttpRequest();
		} else {
			// IE6, IE5 浏览器执行代码
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var str = "";
				str = xmlhttp.responseText;
				checkIdd = str;
				console.log(checkIdd);
				alert("验证码已发送至手机！");
			}
	
		}
		xmlhttp.open("POST", "http://localhost/MVC/public/index.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("keyPhone=" + keyPhone + "&action=sendms");
		settime(val);
	}
    
	var strCodee=1;
    function checkKeyCode(){
    	
    	var keyCode= $("#keyCode").val();
    	
    	var checkId=checkIdd;
    			
    	var xmlhttp;
    	if (window.XMLHttpRequest) {
    		// IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
    		xmlhttp = new XMLHttpRequest();
    	} else {
    		// IE6, IE5 浏览器执行代码
    		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    	}
    	xmlhttp.onreadystatechange = function() {
    		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
    			// var str = "";
    			strCodee = xmlhttp.responseText;
    			// alert(str);
    			console.log(strCodee);
    			if(strCodee==1){
    				// alert("短信验证码正确");					
    			}else if(strCodee==2){
    				// alert("短信验证码错误");					
    			}else{
    				// alert("短信验证码已失效");					
    			}	
    		}
    		
    	}
    	// xmlhttp.open("POST", "http://localhost/MVC/public/index.php", true);
		xmlhttp.open("POST", "http://ezcteam.xyz/ezcphp/test/public/index.php", true);
    	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    	xmlhttp.send("keyCode=" + keyCode + "&action=checkCode"+"&checkId="+checkId);		
    }
	
	var countdown = 60;	
	function settime(val) {
		if (countdown == 0) {
			val.removeAttribute("disabled");
			val.value = "免费获取验证码";
			countdown = 60;
			return;
		} else {
			val.setAttribute("disabled", true);
			val.value = "重新发送码(" + countdown + ")";
			countdown--;
		}
		setTimeout(function() {
			settime(val)
		}, 1000)
	}
    //检查验证码是否正确
    // function validateCode()
    // {
    //     //获取显示区生成的验证码
    //     var checkCode = document.getElementById("checkCode").innerHTML;
    //     //获取输入的验证码
    //     var inputCode = document.getElementById("inputCode").value;
    //     console.log(checkCode);
    //     console.log(inputCode);
				// if (inputCode.length <= 0)
				// {
				//     // alert("请输入验证码！");					
				// 	$(".signCode span").text("请输入验证码！");					
				// }
				// else if (inputCode.toUpperCase() != checkCode.toUpperCase())
				// {
				//     // alert("验证码输入有误！");
				// 	$(".signCode span").text("验证码输入有误！");
				//     createCode(5);
				// }
				// else
				// {
				//     // alert("验证码正确！");
				// 	$(".signCode span").text("验证码正确！");
				// }
       
    // } 
	
	$("#inputCode").blur(function() {
		//获取显示区生成的验证码
		var checkCode = document.getElementById("checkCode").innerHTML;
		//获取输入的验证码
		var inputCode = document.getElementById("inputCode").value;
		// console.log(checkCode);
		// console.log(inputCode);
			if (inputCode.length <= 0)
			{
			    // alert("请输入验证码！");					
				$(".signCode span").text("请输入验证码！");				
			}
			else if (inputCode.toUpperCase() != checkCode.toUpperCase())
			{
			    // alert("验证码输入有误！");
				$(".signCode span").text("验证码输入有误！");
			    createCode(5);
			}
			else
			{
			    // alert("验证码正确！");
				$(".signCode span").text("验证码正确！");
			}
		})
		
		$(".confirm").click(function() {
			// validateCode();
			// checkKeyCode();
			var strCode=window.strCodee;
			var name = $("#name").val();
			var psd = $("#pass").val();
			var phe=$("#phone").val();
			var action = $("#actionReg").val();
			console.log("strCode="+strCode);
			var checkname=$("#usn").text();
			var checkpwd=$("#pwd").text();
			var checkpwdc=$("#checkpwd").text();
			var checkphe=$("#phe").text();
			var checkCode=$("#codec").text();
			
			
			var xmlhttp;
			if(checkpwdc=="密码一致"&&checkpwd=="符合要求"&&checkname=="符合要求"&&checkphe=="符合要求"&&checkCode=="验证码正确！"&&strCodee==1){
			if (window.XMLHttpRequest) {
				// IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
				xmlhttp = new XMLHttpRequest();
			} else {
				// IE6, IE5 浏览器执行代码
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					var str = "";
					str = xmlhttp.responseText;	
						alert(str);
						// console.log(str);
					if (str == "注册成功") {					
						window.location = "login.html";
					}
				}
			}
			xmlhttp.open("POST", "http://ezcteam.xyz/ezcphp/test/public/index.php", true);
			// xmlhttp.open("POST", "http://ezcteam.xyz/ezcphp/test/public/index.php", true);
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("name=" + name + "&pwd=" + psd + "&action=" + action+"&phone="+phe);
			}else{
				// alert("输入有误，请重新输入！");
			}
			
		})
		
		//记住用户名密码
		        function Save() {
		            if ($("#rememberMe").prop("checked")) {
		                var str_username = $("#username").val();//用户名
		                var str_password = $("#password").val();//密码
		                $.cookie("rmbUser", "true", { expires: 7 }); //存储一个带7天期限的cookie
		                $.cookie("username", str_username, { expires: 7 });
		                $.cookie("password", str_password, { expires: 7 });
		            }
		            else {
		                $.cookie("rmbUser", "false", { expire: -1 });
		                $.cookie("username", "", { expires: -1 });
		                $.cookie("password", "", { expires: -1 });
		            }
		 
		            if ($("#autoLogin").prop("checked")) {
		                var str_username = $("#username").val();
		                var str_password = $("#password").val();
		                $.cookie("auto", "true", { expires: 7 }); //存储一个带7天期限的cookie
		                $.cookie("username", str_username, { expires: 7 });
		                $.cookie("password", str_password, { expires: 7 });
		            }
		            else {
		                $.cookie("auto", "false", { expire: -1 });
		                $.cookie("username", "", { expires: -1 });
		                $.cookie("password", "", { expires: -1 });
		            }
		        };
				$(document).ready(function () {
				            $("#autoLogin").change(function() {
				                if($("#autoLogin").prop("checked")){
				                    $.cookie("auto", "true", { expires: 7 });
				                }else{
				                    $.cookie("auto", "false", { expires: 7 });
				                }
				            });
				 
				            if ($.cookie("rmbUser") == "true") {
				                $("#rememberMe").attr("checked", true);
				                $("#username").val($.cookie("username"));
				                $("#password").val($.cookie("password"));
				            }
				 
				            if ($.cookie("auto") == "true") {
				                setTimeout(function(){
				                    if($.cookie("auto") == "true")
				                    window.location.href='下一个界面';
				                },5000);
				            }
				});